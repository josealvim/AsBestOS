section .mb_header
multiboot_header:
	.magic: equ 0xe85250d6
	.size:  equ .end - $ 

	dd .magic
	dd 0x00000000
	dd .size
	dd 0x100000000 - (.magic + 0 + .size)
	
	dw 0
	dw 0 
	dd 8

	.end:   equ $

section .kstack
align 0x1000
stack_bot:
	resb 0x1000 * 16
stack_top:

section .bss
align 0x1000

l4pt:
	resb 0x1000
l3pt:
	resb 0x1000
l2pt:
	resb 0x1000

section .rodata
gdt64:
	.nulls:
	dq 0
	.kcode: equ $ - gdt64
	dq (1 << 43) | (1 << 44) | (1 << 47) | (1 << 53)
.ptr:
	dw $ - gdt64 - 1
	dq gdt64

section .text
bits 32
global _start_x86_64
; extern kini 

_start_x86_64:
	mov esp, stack_top

	call check_mboot
	call check_cpuid
	call check_lmode

	call make_idmap
	call enter_lmode

	lgdt [gdt64.ptr]
	jmp   gdt64.kcode:_64bit_mode

	boot_err:
		mov dword [0xb8000], 0x4f524f45 
		mov dword [0xb8004], 0x4f3a4f52
		mov dword [0xb8008], 0x4f204f20
		mov byte  [0xb800a], al
		hlt
		jmp boot_err


check_mboot:
	; multiboot should set eax to magic 
	cmp eax, 0x36D76289
	jne .mboot_fail
	ret
	.mboot_fail:
		mov al, "M"
		jmp boot_err

check_cpuid:
	; read flag register 
	pushfd
	pop eax

	; save it in  
	mov ecx, eax

	; try to write to bit 21 
	xor  eax, 1 << 21
	push eax
	popfd

	; to see if write went through 
	pushfd
	pop  eax
	
	; restore flag bits 
	push ecx
	popfd

	; compare old vs. new 
	cmp eax, ecx
	je .cpuid_fail
	ret
	.cpuid_fail:
		mov al, "C"
		jmp boot_err

check_lmode:
	mov eax, (1 << 31)
	cpuid
	cmp eax, (1 << 31) + 1
	jb .no_epi

	mov eax, (1 << 31) + 1
	cpuid 
	test edx, 1 << 29
	jz .no_lmode

	ret

	.no_epi:
		mov al, "E"
		jmp boot_err
	.no_lmode:
		mov al, "L"
		jmp boot_err

make_idmap:
	mov eax, l3pt
	or  eax, 0b11
	mov [l4pt + 0], eax

	mov eax, l2pt
	or  eax, 0b11
	mov [l3pt + 0], eax

	xor ecx, ecx
	.next_page:
		mov eax, 0x2000 * 0x1000 ; 2MB 
		mul ecx
		or  eax, 0b10000011
		mov [l2pt + ecx * 8], eax

		inc ecx
		cmp ecx, 512
	jne .next_page
	ret

enter_lmode:
	; tell CPU where the l4 page table is 
	mov eax, l4pt
	mov cr3, eax

	; set cr4 bit 5, PAE bit 
	mov eax, cr4
	or  eax, 1 << 5
	mov cr4, eax

	; read model specific register (into eax) 
	mov ecx, 0xC0000080
	rdmsr

	; set the msr bit 8  
	or  eax, 1 << 8
	wrmsr

	; enable paging
	mov eax, cr0
	or  eax, 1 << 31
	mov cr0, eax

	ret

bits 64 
extern kentry
_64bit_mode:
	; null out the segment registers
	mov ax, 0
	mov ss, ax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov dword [0xB8000], 0x2F4B2F4F
	call kentry

	._catch:
		cli
		hlt
		jmp ._catch