LD = x86_64-elf-ld
LD_FLAGS  = -n

CXX = x86_64-elf-g++
CXX_FLAGS = -O2 -g			\
	-ffreestanding			\
	-fno-use-cxa-atexit		\
	-fno-builtin			\
	-fno-rtti				\
	-fno-exceptions			\
	-fno-leading-underscore	\
	-mno-red-zone			\
	-nostdlib

ASM = nasm
ASM_FLAGS = -f elf64

KERNEL_SOURCES = $(shell find kernel -type f -name "*.cpp")
KERNEL_OBJECTS = ${KERNEL_SOURCES:.cpp=.o}

%.o: %.cpp
	${CXX} ${CXX_FLAGS} -c -o $@ $<

%.o: %.asm
	${ASM} ${ASM_FLAGS}    -o $@ $<

i86pc = /usr/lib/grub/i386-pc
arch/%/kern.iso: arch/%/kern.elf
	cp $< ${<D}/iso/boot
	grub-mkrescue ${i86pc} -o $@ arch/x86_64/iso

arch/%/kern.elf: arch/%/boot.o ${KERNEL_OBJECTS}
	${LD}  ${LD_FLAGS}     -o $@ $? -T ${@D}/script.ld 

.PHONY: x86_64 clean run docker-tool docker-run

x86_64: arch/x86_64/kern.iso
	@ echo done!

clean:
	find . -type f -name "*.o"   -exec rm "{}" \;
	find . -type f -name "*.iso" -exec rm "{}" \;
	find . -type f -name "*.elf" -exec rm "{}" \;

run: 
	qemu-system-x86_64 -serial mon:stdio -cdrom arch/x86_64/kern.iso

docker_tool:
	docker build toolchain -t $@

docker-run:
	docker run --rm -it -v `pwd`:/root/AsbestOS docker_tool